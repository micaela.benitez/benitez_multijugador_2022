﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Launcher.cs" company="Exit Games GmbH">
//   Part of: Photon Unity Networking Demos
// </copyright>
// <summary>
//  Used in "PUN Basic tutorial" to handle typical game management requirements
// </summary>
// <author>developer@exitgames.com</author>
// --------------------------------------------------------------------------------------------------------------------

using UnityEngine;
using UnityEngine.SceneManagement;
using Photon.Realtime;
namespace Photon.Pun.Demo.PunBasics
{
#pragma warning disable 649

	/// <summary>
	/// Game manager.
	/// Connects and watch Photon Status, Instantiate Player
	/// Deals with quiting the room and the game
	/// Deals with level loading (outside the in room synchronization)
	/// </summary>
	public class GameManager : MonoBehaviourPunCallbacks
	{

		#region Public Fields

		static public GameManager Instance;

		#endregion

		#region Private Fields

		private GameObject instance;

		[Tooltip("The prefab to use for representing the player")]
		[SerializeField]

		private GameObject playerPrefab;

		#endregion

		#region MonoBehaviour CallBacks

		public string auto;
		public int usuarios; // Cargamos la cantidad de usuarios conectados
		public int usuario;
		public Transform[] pos; // Array objs de posicion inicial
		public Transform init;
		public Transform autoSpwn;


		void Start()
		{
			Instance = this;

			// in case we started this demo with the wrong scene being active, simply load the menu scene
			if (!PhotonNetwork.IsConnected)
			{
				SceneManager.LoadScene("lobby");
				return;
			}

			if (playerPrefab == null)
			{ // #Tip Never assume public properties of Components are filled up properly, always check and inform the developer of it.

				Debug.LogError("<Color=Red><b>Missing</b></Color> playerPrefab Reference. Please set it up in GameObject 'Game Manager'", this);
			}
			else
			{


				if (PlayerManager.LocalPlayerInstance == null)
				{


					// SALA = sala de photon (virtual me asinga VIEWID)
					// SCENA= donde instancio mi jugador.

					// cuento la cantidad de usuarios antes de meterme en la sala
					usuarios = PhotonNetwork.CountOfPlayers;

					// me asigno un numero de usuario tomando el ul8itmo de la lista 
					usuario = usuarios;

					// Me instancio en la sala  // cada usuario nuevo se instancia en una posicion siguiente sumada por playersCount 
					//		GameObject Player =PhotonNetwork.Instantiate(this.playerPrefab.name, pos[usuarios].transform.position, Quaternion.identity, 0);
					GameObject Player = PhotonNetwork.Instantiate("PlayerGTA", init.transform.position, Quaternion.identity, 0);

					// ME FIJO SI SOY MASTER O HOST E INSTANCIO UN AUTO EN LA POSICION AUSPAWN

					if (PhotonNetwork.IsMasterClient)
					{
						GameObject Auto1 = PhotonNetwork.Instantiate("Auto1", autoSpwn.transform.position, Quaternion.identity, 0);
					}

				}
				else
				{

					Debug.LogFormat("Ignoring scene load for {0}", SceneManagerHelper.ActiveSceneName);
				}
			}

		}

		/// <summary>
		/// MonoBehaviour method called on GameObject by Unity on every frame.
		/// </summary>
		void Update()
		{
			if (PhotonNetwork.IsMasterClient)
            {
				//host();
            }

			// "back" button of phone equals "Escape". quit app if that's pressed
			if (Input.GetKeyDown(KeyCode.Escape))
			{
				QuitApplication();
			}
		}

		#endregion

		#region Photon Callbacks

		/// <summary>
		/// Called when a Photon Player got connected. We need to then load a bigger scene.
		/// </summary>
		/// <param name="other">Other.</param>



		public override void OnPlayerEnteredRoom(Player other)
		{
			usuarios = PhotonNetwork.CountOfPlayers;
			Debug.Log("OnPlayerEnteredRoom() " + other.NickName); // not seen if you're the player connecting

			if (PhotonNetwork.IsMasterClient)
			{
				Debug.LogFormat("OnPlayerEnteredRoom IsMasterClient {0}", PhotonNetwork.IsMasterClient); // called before OnPlayerLeftRoom
																										 //	LoadArena();
			}
		}




		/// <summary>
		/// Called when a Photon Player got disconnected. We need to load a smaller scene.
		/// </summary>
		/// <param name="other">Other.</param>
		public override void OnPlayerLeftRoom(Player other)
		{
			usuarios = PhotonNetwork.CountOfPlayers;

			Debug.Log("OnPlayerLeftRoom() " + other.NickName); // seen when other disconnects

			if (PhotonNetwork.IsMasterClient)
			{
				Debug.LogFormat("OnPlayerEnteredRoom IsMasterClient {0}", PhotonNetwork.IsMasterClient); // called before OnPlayerLeftRoom
																										 //	LoadArena(); 
			}
		}

		/// <summary>
		/// Called when the local player left the room. We need to load the launcher scene.
		/// </summary>
		public override void OnLeftRoom()
		{
			SceneManager.LoadScene("lobby"); // Cuando salimos con boton, nos carga la escena lobby de nuevo

		}

		#endregion

		#region Public Methods

		public void LeaveRoom()
		{
			PhotonNetwork.LeaveRoom();
		}

		public void QuitApplication()
		{
			Application.Quit();
		}

		#endregion

		#region Private Methods

		void LoadArena()
		{
			if (!PhotonNetwork.IsMasterClient)
			{
				Debug.LogError("PhotonNetwork : Trying to Load a level but we are not the master Client");
			}

			Debug.LogFormat("PhotonNetwork : Loading Level : {0}", PhotonNetwork.CurrentRoom.PlayerCount);

			PhotonNetwork.LoadLevel("PunBasics-Room for " + PhotonNetwork.CurrentRoom.PlayerCount);
		}
		#endregion
	}


}