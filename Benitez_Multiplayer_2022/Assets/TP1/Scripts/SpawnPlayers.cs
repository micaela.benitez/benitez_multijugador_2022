using UnityEngine;
using Photon.Pun;

public class SpawnPlayers : MonoBehaviour
{
    /// El prefab del jugador a instanciar para cara cliente
    public GameObject playerPrefab = null;

    /// Minimas y maximas posiciones para instanciar al personaje
    public float minX = 0;
    public float maxX = 0;
    public float minY = 0;
    public float maxY = 0;

    /// <summary>
    /// Instancia los personajes en posiciones random entre un minimo y un maximo
    /// </summary>
    private void Start()
    {
        Vector2 randomPosition = new Vector2(Random.Range(minX, maxX), Random.Range(minY, maxY));
        PhotonNetwork.Instantiate(playerPrefab.name, randomPosition, Quaternion.identity);
    }
}
