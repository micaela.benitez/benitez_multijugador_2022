using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    /// Padre de todas las monedas a recoger
    public GameObject coinsParent = null;

    /// Cantidad total de monedas disponibles actualmente
    private int totalCoinsAvailable = 0;

    /// <summary>
    /// Chequea si el juego termino o no
    /// </summary>
    private void Update()
    {
        GetTotalCoinsAvailable();

        if (totalCoinsAvailable == 0)
        {
            SceneManager.LoadScene("Lobby");
        }
    }

    /// <summary>
    /// Chequea cuantas monedas quedan disponibles actualmente
    /// </summary>
    private void GetTotalCoinsAvailable()
    {
        totalCoinsAvailable = 0;

        foreach (Transform childer in coinsParent.transform)
        {
            totalCoinsAvailable++;
        }
    }

    //private IEnumerator FinishGame()
    //{
    //    
    //}
}
