using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

[RequireComponent(typeof(InputField))]
public class PlayerName : MonoBehaviour
{
	/// Guarda el nombre del personaje del cliente
	private const string playerNamePrefKey = "PlayerName";

	/// <summary>
	/// Guarda el nickname en PhotonNetwork.NickName
	/// </summary>
	private void Start()
	{
		string defaultName = string.Empty;
		InputField inputField = GetComponent<InputField>();

		if (inputField)
		{
			if (PlayerPrefs.HasKey(playerNamePrefKey))
			{
				defaultName = PlayerPrefs.GetString(playerNamePrefKey);
				inputField.text = defaultName;
			}
		}

		PhotonNetwork.NickName = defaultName;
	}

	/// <summary>
	/// Establece el nombre del jugador y lo guarda en PlayerPrefs para futuras sesiones
	/// </summary>
	public void SetPlayerName(string value)
	{
		if (string.IsNullOrEmpty(value))
		{
			Debug.LogError("Player Name is null or empty");
			return;
		}

		PhotonNetwork.NickName = value;
		PlayerPrefs.SetString(playerNamePrefKey, value);
	}
}