using System.Collections;
using UnityEngine;
using Photon.Pun;
using TMPro;

public class Player : MonoBehaviourPunCallbacks
{
    [Header("Nickname")]
    /// El texto con el nickname
    public TMP_Text nicknameText = null;

    [Header("Move data")]
    /// Velocidad de movimiento horizontal
    public float moveSpeed = 0;

    [Header("Jump data")]
    /// Velocidad de salto
    public float jumpUpSpeed = 0;
    /// N�mero de saltos seguidos permitidos
    public int allowJumpTimes = 0;
    /// Aceleraci�n descendente m�ltiple
    public float fallGravityMultiplier = 2;
    /// La layer del suelo
    public LayerMask whatIsGround = 0;
    /// Posici�n inferior del personaje
    public Transform groundCheck = null;

    [Header("Efectos")]
    /// Efecto de salto de aire
    public GameObject cloud = null;

    /// Input horizontal
    private float inputHorizontal = 0;
    /// Si presionar la tecla de salto
    private bool jumpPressed = false;
    /// Conteo de los saltos actuales
    private int airJumpCount = 0;
    /// Si el jugador mira a la derecha o no
    private bool faceRight = true;
    /// Determina si el jugador toca el suelo
    private bool isGrounded = false;
    /// Radio de detecci�n de toma de contacto
    private float checkRadius = 0.15f;
    /// Nickname del personaje del cliente
    private string nickname = null;

    /// Game object de las coins a recoger (cambiado a cherrys)
    private GameObject coin = null;
    /// El game object ScoreManager de la escena
    private ScoreManager scoreManager = null;
    /// Parametro de PhotonView
    private PhotonView view = null;
    /// Rigid body del jugador
    private Rigidbody2D rigidBody = null;
    /// Animator del jugador
    private Animator animator = null;    

    /// <summary>
    /// Inicializo los parametros privados
    /// </summary>
    private void Start()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        scoreManager = FindObjectOfType<ScoreManager>();
        view = GetComponent<PhotonView>();
        airJumpCount = 0;
        if (cloud) cloud.SetActive(false);

        if (view.IsMine)
        {
            nickname = PhotonNetwork.NickName;
            /// Mando mi nickname por RPC a todos los clientes, incluidos a los que entren mas tarde
            photonView.RPC("NickRPC", RpcTarget.AllBufferedViaServer, nickname);
        }
    }

    /// <summary>
    /// Si el jugador es local obtiene su input
    /// </summary>
    private void Update()
    {
        if (view.IsMine)
        {
            inputHorizontal = Input.GetAxisRaw("Horizontal");
            if (Input.GetButtonDown("Jump")) jumpPressed = true;
        }
    }

    /// <summary>
    /// Si el jugador es local actualiza su input y su animator. Asi no maneja a los demas remotos
    /// </summary>
    private void FixedUpdate()
    {
        if (view.IsMine)
        {
            Move();
            Jump();
            SwitchAnimState();

            isGrounded = Physics2D.OverlapCircle(groundCheck.position, checkRadius, whatIsGround);
            if (isGrounded) airJumpCount = 0;

            if (rigidBody.velocity.y < 0) rigidBody.velocity += Vector2.up * Physics2D.gravity.y * (fallGravityMultiplier - 1) * Time.fixedDeltaTime;
        }
    }

    /// <summary>
    /// Movimiento horizontal del personaje
    /// </summary>
    private void Move()
    {
        rigidBody.velocity = new Vector2(inputHorizontal * moveSpeed, rigidBody.velocity.y);
        if (inputHorizontal > 0 && !faceRight || inputHorizontal < 0 && faceRight) Flip();        
    }

    /// <summary>
    /// Salto del personaje
    /// </summary>
    private void Jump()
    {
        if (jumpPressed)
        {
            jumpPressed = false;
            if (allowJumpTimes <= 0)
            {
                /// No se permite saltar
                return;
            }
            else if (isGrounded && airJumpCount == 0)
            {
                /// Salta del suelo
                rigidBody.velocity = Vector2.up * jumpUpSpeed;
            }
            else if (airJumpCount < allowJumpTimes - 1)
            {
                /// Salta en el aire
                rigidBody.velocity = Vector2.up * jumpUpSpeed;
                airJumpCount++;
                /// Activa el efecto de explosi�n de una nube al saltar en el aire
                if (cloud) StartCoroutine(SpawnCloud());
            }
        }
    }

    /// <summary>
    /// Rota horizontalmente el sprite del jugador dependiendo si va para la izquierda o derecha
    /// </summary>
    private void Flip()
    {
        faceRight = !faceRight;

        Vector3 scale = transform.localScale;
        scale.x *= -1;
        transform.localScale = scale;

        Vector3 nicknameScale = nicknameText.rectTransform.localScale;
        nicknameScale.x *= -1;
        nicknameText.rectTransform.localScale = nicknameScale;

        /// Mando mi flip por RPC a todos los remotos
        photonView.RPC("FlipRPC", RpcTarget.Others, scale, nicknameScale);
    }

    /// <summary>
    /// Actualiza las animaciones del personaje
    /// </summary>
    private void SwitchAnimState()
    {
        /// Correr
        if (inputHorizontal == 0) animator.SetBool("Running", false);
        else animator.SetBool("Running", true);

        /// Salto
        animator.SetFloat("SpeedH", Mathf.Abs(rigidBody.velocity.x));
        animator.SetFloat("SpeedV", rigidBody.velocity.y);

        /// Si esta tocando el piso
        animator.SetBool("IsGrounded", isGrounded);
    }

    /// <summary>
    /// Efecto de la nube cuando salta
    /// </summary>
    private IEnumerator SpawnCloud()
    {
        cloud.SetActive(true);
        cloud.GetComponent<Animator>().SetTrigger("Explode");
        yield return new WaitForSeconds(1);
        cloud.SetActive(false);
    }

    /// <summary>
    /// Si choco con una moneja (cambiado a cherrys), actualizo el puntaje
    /// </summary>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Coin")
        {
            coin = collision.gameObject;
            Destroy(coin);
            if (view.IsMine) scoreManager.AddToScore();
        }
    }

    ///////////////////////////////////////////////////////////// Funciones RPC /////////////////////////////////////////////////////////////
    /// <summary>
    /// Mando por RPC el nickname del personaje
    /// </summary>
    [PunRPC]
    private void NickRPC(string nickname)
    {
        nicknameText.text = nickname;
    }

    /// <summary>
    /// Mando por RPC el flip del personaje
    /// </summary>
    [PunRPC]
    private void FlipRPC(Vector3 playerScale, Vector3 nicknameScale)
    {
        transform.localScale = playerScale;
        nicknameText.rectTransform.localScale = nicknameScale;
    }
}
