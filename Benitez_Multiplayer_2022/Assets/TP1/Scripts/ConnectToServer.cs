using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Photon.Pun;


/***************************************************************************************************************************
******************************************* ARQUITECTURA DE RED DE ESTE PROYECTO ******************************************* 

En este proyecto la arquitectura de red no tiene un dedicado, no tiene un servidor ejecutando la aplicacion sin interactuar, 
sin ser cliente. Sino que el servidor (host) es tambien el cliente, y cualquiera de nosotros puede ser host. En este caso 
seria el primero que abre la sala.

Entonces, nuestra arquitectura de red es host-client, no dedicated-host. El dedicado seria una computadora con buenos componentes 
y seguridad que este dedicada exclusivamente a alojar usuarios y hacerlos interactuar. 
****************************************************************************************************************************/


public class ConnectToServer : MonoBehaviourPunCallbacks
{
    /// Input field donde escribimos el nombre del jugador
    public InputField nameInput = null;
    /// Texto que muestra la carga
    public Text loadingText = null;

    /// <summary>
    /// Initicializa el texto de carga
    /// </summary>
    private void Start()
    {
        loadingText.gameObject.SetActive(false);
        
        /// Esto asegura que podamos usar PhotonNetwork.LoadLevel() en el host y que todos los clientes en la misma sala sincronicen su nivel automaticamente
        PhotonNetwork.AutomaticallySyncScene = true;
    }

    /// <summary>
    /// Primero nos conectamos al Photon Server
    /// </summary>
    public void ConnectPhotonServer()
    {
        if (nameInput.text != "")
        {
            loadingText.gameObject.SetActive(true);
            PhotonNetwork.ConnectUsingSettings();
        }
        else
        {
            Debug.LogError("Ingrese un nombre!");
        }
    }

    /// <summary>
    /// Una vez conectados al Photon Server, vamos al lobby
    /// </summary>
    public override void OnConnectedToMaster()
    {
        base.OnConnectedToMaster();
        PhotonNetwork.JoinLobby();
    }

    /// <summary>
    /// Una vez que entramos en el lobby, cargamos la escena del lobby
    /// </summary>
    public override void OnJoinedLobby()
    {
        base.OnJoinedLobby();
        SceneManager.LoadScene("Lobby");
    }
}