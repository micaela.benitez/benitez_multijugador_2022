using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    /// El texto de la UI con el puntaje
    public Text scoreText = null;

    /// El puntaje actual
    private int totalScore = 0;

    /// <summary>
    /// Llamo a esta funcion para actualizar el puntaje
    /// </summary>
    public void AddToScore()
    {
        totalScore++;
        scoreText.text = totalScore.ToString();
    }
}
