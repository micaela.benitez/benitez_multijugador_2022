using UnityEngine.UI;
using Photon.Pun;

public class CreateAndJoinRooms : MonoBehaviourPunCallbacks
{
    /// Input field donde escribimos el nombre de la sala a crear
    public InputField createInput = null;
    /// Input field donde escribimos el nombre de la sala a unirnos
    public InputField joinInput = null;

    /// <summary>
    /// Creamos la sala
    /// </summary>
    public void CreateRoom()
    {
        PhotonNetwork.CreateRoom(createInput.text);
    }

    /// <summary>
    /// Nos unimos a la sala creada anteriormente
    /// </summary>
    public void JoinRoom()
    {
        PhotonNetwork.JoinRoom(joinInput.text);
    }

    /// <summary>
    /// Cargamos la escena de juego una vez que nos unimos a la sala creada
    /// </summary>
    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        PhotonNetwork.LoadLevel("Game");
    }
}
