using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;

namespace Common
{
    namespace Photon.Pun.Demo.PunBasics
    {
        public class Play : MonoBehaviourPunCallbacks
        {
            public Vector3 scr = new Vector3(0f, 30f, 0f);//  Texto de nuestro nick
            public GameObject Canvas;
            public GameObject nickText;
            ///////////////////////////////////////////////////////////////////////

            public string nombre; // nombre de photon o nickname
            public bool control; // control player o remoto

            public Text nick;
            public float velo; // velocidad del player

            void Start()
            {

                if (!photonView.IsMine)
                {// Chequeo si soy player "soy local" o remoto
                    control = false;
                }
                else
                {                 // de lo contrario "SOY LOCAL", 
                    control = true;



                    nombre = PhotonNetwork.NickName; // adquiero de PHOTON mi nick name


                    // nickname de texto sobre mi cabeza...
                    GameObject Nick = Instantiate(nickText, transform.position, Quaternion.identity);
                    Nick.transform.SetParent(Canvas.transform);
                    nick = GameObject.Find("Nick").GetComponent<Text>();
                    nick.text = "" + nombre;
                    ////////////////////////////////////////////////////////

                    // Es el que actualiza mi posicion y nickname todo el tiempo
                    photonView.RPC("nombreRPC", RpcTarget.AllBuffered, nombre);

                }

            }

            void Update()
            {

                //nick.transform.position=new Vector3(transform.position.x,transform.position.y,1);


                if (control == true)
                {
                    controlS();
                    GetComponent<Rigidbody>().isKinematic = true; // Desabilito el rigidbody en su peso o gravedad
                }
                else
                {
                    GetComponent<Rigidbody>().isKinematic = true; // Desabilito el rigidbody en su peso o gravedad
                }


                // 
                nick.transform.position = Camera.main.WorldToScreenPoint(this.gameObject.transform.position) + scr;

            }

            void controlS()
            {
                transform.Translate(Vector3.forward * Time.deltaTime * (Input.GetAxis("Vertical") * velo));
                transform.Rotate(0, Input.GetAxis("Horizontal"), 0);

            }


            [PunRPC]

            void nombreRPC(string Nombre)
            {
                gameObject.name = "" + Nombre;

            }

        }
    }
}