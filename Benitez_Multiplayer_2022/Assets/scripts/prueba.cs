using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

// Sistema de movimiento por RPC

namespace Common
{
    public class prueba : MonoBehaviourPunCallbacks
    {
        int vida = 0;
        private int mov;

        // Funcion que detecta colisiones
        //{
        // Chequeo si el tag (categoria) del objeto es bala
        // Si es asi, chequeo si soy remoto !isMine, me quito puntos de vida y le aviso a
        // mi local que recibi un disparo de el usuario ID de photon 1001, y me quito mi vida en mi local.
        // ESTO LO HAGO VIA PCR
        //}

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.tag == "bala" && !photonView.IsMine)
            {
                vida -= 1;
                photonView.RPC("balaRPC", RpcTarget.Others, 1);
            }
        }

        void Update()
        {


            // RPCS DE MENSAJERIA
            if (Input.GetKeyDown(KeyCode.M))
            {
                // Buffered crea un buffer po memoria para mensajesa anteriores  
                photonView.RPC("mensajeRPC", RpcTarget.AllBuffered, "recibido: " + PhotonNetwork.NickName); // Agrego al mensaje "recibido" mi nickname de photon y lo envio por RPC

                photonView.RPC("destroyRPC", RpcTarget.All, 1); // Agrego al mensaje "recibido" mi nickname de photon y lo envio por RPC


                /////////////////////////////////////////////////

            }


            // RPCS DE MOVIMIENTOS O ACCIONES  ///////////
            /*
              if(Input.GetKeyDown(KeyCode.W)) {
              mov=1;
              photonView.RPC("movimientoRPC", RpcTarget.AllBuffered, ""+mov); // Agrego al mensaje "recibido" mi nickname de photon y lo envio por RPC


             // 
              photonView.RPC("coloresRPC", RpcTarget.others, ""+color); 


              }else if(Input.GetKeyDown(KeyCode.S)) {
              mov=-1;
              photonView.RPC("movimientoRPC", RpcTarget.AllBuffered, ""+mov); // Agrego al mensaje "recibido" mi nickname de photon y lo envio por RPC}
              }
              //////////////////////////////////////////
    */


        }

        // catergoria mensajes
        [PunRPC]  // FUNCION RPC :   LLAMADA A PROCEDIMIENTO REMOTO ::::::::::::::::::
        void mensajeRPC(string mensaje)
        { //variable String mensaje , recibe lo que enviamos por RPC
            if (!photonView.IsMine)
                Debug.Log("mensaje: " + mensaje);

            // Chequeo si soy player "soy local" o remoto
        }


        // categoria Acciones

        [PunRPC]  // FUNCION RPC :   LLAMADA A PROCEDIMIENTO REMOTO ::::::::::::::::::

        void movimientoRPC(int variable)
        {           //variable String mensaje , recibe lo que enviamos por RPC
            transform.Translate(Vector3.forward * Time.deltaTime * variable);
            Debug.Log("mensdajhe" + variable);
        }

        [PunRPC]  // FUNCION RPC :   LLAMADA A PROCEDIMIENTO REMOTO ::::::::::::::::::

        void destroyRPC(int a)
        {
            if (a == 1)
                Destroy(gameObject);

        }

        void balaRPC(int a)
        {
            if (a == 1 && photonView.IsMine)
                vida -= 1;
        }


    }
}